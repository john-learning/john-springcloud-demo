package john.springcloud.demo.rocketmq.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author : john
 * @date : 2020-04-11 23:45:17
 */
@Service
public class ParamConfigService {
    @Value("${rocket.group}")
    public String rocketGroup;
    @Value("${rocket.topic}")
    public String rocketTopic;
    @Value("${rocket.tag}")
    public String rocketTag;

    @Value("${fee-plat.fee-plat-group}")
    public String feePlatGroup;
    @Value("${fee-plat.fee-plat-topic}")
    public String feePlatTopic;
    @Value("${fee-plat.fee-account-tag}")
    public String feeAccountTag;
}
