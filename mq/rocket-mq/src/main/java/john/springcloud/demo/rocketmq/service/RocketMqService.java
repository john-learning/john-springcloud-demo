package john.springcloud.demo.rocketmq.service;

import org.apache.rocketmq.client.producer.SendResult;

/**
 * @author : john
 * @date : 2020-04-11 23:49:44
 */
public interface RocketMqService {
    SendResult openAccountMsg(String msgInfo);
}
