package john.springcloud.demo.rocketmq.controller;

import john.springcloud.demo.rocketmq.service.RocketMqService;
import org.apache.rocketmq.client.producer.SendResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author : john
 * @date : 2020-04-11 23:49:31
 */
@RestController
public class RocketController {
    @Resource
    private RocketMqService rocketMqService;

    @RequestMapping("/sendMsg")
    public SendResult sendMsg() {
        String msg = "OpenAccount Msg";
        SendResult sendResult = null;
        try {
            sendResult = rocketMqService.openAccountMsg(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendResult;
    }
}
